package com.kurdestanbootcamp.order.model;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.kurdestanbootcamp.order.R;


/**
 * Created by Shima on 10/23/2017.
 */



public class ProductViewHolder extends RecyclerView.ViewHolder {




    public TextView product_name ;
    public TextView price ;
    public TextView productid ;
    public TextView userId ;



    public ProductViewHolder(View convertView ) {
        super(convertView);


        this. product_name= (TextView) convertView.findViewById(R.id.textname);
        this. price=(TextView) convertView.findViewById(R.id.textprice);
        this. productid=(TextView) convertView.findViewById(R.id.textproductid);



    }
}
