package com.kurdestanbootcamp.order.services;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.RequestFuture;
import com.kurdestanbootcamp.order.ProductActivity;
import com.kurdestanbootcamp.order.StoreActivity;
import com.kurdestanbootcamp.order.app.AppController;
import com.kurdestanbootcamp.order.model.Product;
import com.kurdestanbootcamp.order.model.ProductRecyclerAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shima on 10/24/2017.
 */


public class GetStoreServices {


    public StoreActivity ctx;

    RecyclerView recyclerView;

    public GetStoreServices(StoreActivity ctx,RecyclerView recyclerView) {
        this.ctx=ctx;
        this.recyclerView=recyclerView;
    }

    public   void getStore() throws JSONException, UnsupportedEncodingException {
        //   http://192.168.1.2:9080/Datasnap/rest/TserverMethods/AddToRecipt/21/1/5/  [{"stuffCode":1001,"entity":1,"unitSellPrice":3,"discount":3,"deficitValue":8,"taxValue":10}, {"stuffCode":1002,"entity":12,"unitSellPrice":40,"discount":32,"deficitValue":5,"taxValue":15}]
        JSONArray obj = null  ;









        //this.factorBeanClass=factorBean;
        //  String   urlGetCategory= Services.getCategories+shopId+'/'+password;
        String   urlStore= Services.Store;
        Log.e("url stores****==", urlStore) ;






        //Log.e("Location isssss",jsLoc.toString()) ;

        // Log.e("Json issss",json.toString()) ;




        JsonArrayRequest jsonObjReq=null;

        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        jsonObjReq = new JsonArrayRequest(Request.Method.GET,
                urlStore, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {



                        if (response!=null){


                            parseJson(response);
                        }





                    }
                    // pDialog.hide();

                }, new Response.ErrorListener() {
            @Override

            public void onErrorResponse(VolleyError error) {
                //   Toast.makeText(context, R.string.error_connect, Toast.LENGTH_SHORT).show();



                if ((error instanceof NetworkError) || (error instanceof NoConnectionError) ) {




                    Toast.makeText(ctx,"Network Error", Toast.LENGTH_SHORT).show();
                    // progressDialog.dismiss();
                    return;
                }
                if (error instanceof TimeoutError){


                    Toast.makeText(ctx, "TimeOut", Toast.LENGTH_SHORT).show();
                    // context.getDialog().dismiss();
                    return;
                }




                if ((error instanceof ServerError) || (error instanceof AuthFailureError)){

                    //  Toast.makeText(context, R.string.server_error, Toast.LENGTH_SHORT).show();
                    // context.getDialog().dismiss();






                    Toast.makeText(ctx, "ServerError", Toast.LENGTH_SHORT).show();
                    return;
                }







            }
        }

        )

        {
            public String getBodyContentType()
            {
                return "application/json";
            }




        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Log.e("Body",new String(jsonObjReq.getBody(), "UTF-8"));
        String tag_json_arry = "json_array_req";
// Adding request to request queue
        // mRequestQueue.add(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_arry);





    }
    public void parseJson(JSONArray response){
        List<Product> productList=new ArrayList<>();

        Log.e("Finito","size="+response.toString());


        for (int i = 0; i < response.length(); i++) {
            // JSONObject row = response.getJSONObject("result");;
            try {


               /* {
                    "houseId": 6,
                        "userId": 5,
                        "city": "تهران",
                        "homeAge": 5,
                        "lat": 35.335224,
                        "lon": 46.98995,
                        "room": 5,
                        "area": 70,
                        "price": 50000000,
                        "createDate": "Nov 9, 2016"
                },*/
                //   Contact c=new Contact();
                JSONObject c=(JSONObject) response.get(i);
                Log.e("objjj","ts="+c.toString());
                long userId=c.getLong("userId");
                long productid=c.getLong("product");
                long price=c.getLong("price");
                String  product_name=c.getString("product_name");



                Product product=new Product();
                product.setuserId(userId);
                product.setPrice(price);
                product.setProduct_name(product_name);
                product.setProductid(productid);



                productList.add(i,product);










            } catch (JSONException e) {
                Log.e("error",e.toString());
                e.printStackTrace();

            }



        }
        ProductRecyclerAdapter adapter=new ProductRecyclerAdapter(ctx,storetList,recyclerView);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
       /* Collections.sort(categoryBeanParent);
        Collections.sort(categoryBeanLisAll);
        Util.allCategoryList=categoryBeanLisAll;
        Util.parentCategoryList=categoryBeanParent;
        CustomCategoryListAdapter adapter=new CustomCategoryListAdapter(context,categoryBeanParent);
        context.getListViewCategory().setAdapter(adapter);
        adapter.notifyDataSetChanged();
*/

        //context.getDialog().dismiss();
        Log.e("Finito","size="+storeList.size());
        Log.e("Finito","Finito");
    }

}

