package com.kurdestanbootcamp.order.model;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kurdestanbootcamp.order.ProductActivity;
import com.kurdestanbootcamp.order.StoreActivity;

import java.util.List;

/**
 * Created by Shima on 10/24/2017.
 */


public class StoreRecyclerAdapter extends RecyclerView.Adapter<StoreViewHolder> {
    private List<Store> storeList;
    private StoreActivity mContext;
    int heightSize;

    RecyclerView recyclerView;
    public StoreRecyclerAdapter(StoreActivity context, List<Product> feedItemList,RecyclerView recyclerView) {
        this.storeList = feedItemList;
        this.mContext = context;
        this.recyclerView=recyclerView;
        setListener();
        //Log.e   ("const adapter","");
    }

    /*   @Override
       public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
          // View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, null);
           View itemView = LayoutInflater.from(viewGroup.getContext())
                   .inflate(R.layout.item, parent, false);



           CustomViewHolder viewHolder = new CustomViewHolder(view,heightSize);
           Log.e   ("CustomViewHolder adapter","");
           return viewHolder;
       }*/
    @Override
    public StoreViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_row, parent, false);


        return new StoreViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final StoreViewHolder viewHolder, final int i) {
        final Store store =storeList.get(i);
        viewHolder.price.setText(store.getPrice()+"تومان");
        viewHolder.product_name.setText(store.getProduct_name()+" نام کالا");
        viewHolder.productid.setText(store.getProductid()+"کد کالا");
        viewHolder.product_name.setText(store.getProduct_name());






    }
    public void setListener(){
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(mContext, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int i) {
                        Store h=  storeList.get(i);

                        Intent intent=new Intent(mContext, ProductActivity.class)  ;
                        //   intent.putExtra("productId",h.getProductId());
                        intent.putExtra("store",h);
                        mContext.startActivity(intent);

                    }
                })
        );


    }


    @Override
    public int getItemCount() {
        return (null != storeList ? storeList.size() : 0);

    }


}
