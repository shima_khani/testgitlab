package com.kurdestanbootcamp.order;

/**
 * Created by Shima on 10/24/2017.
 */

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.kurdestanbootcamp.order.services.GetProductServices;
import com.kurdestanbootcamp.order.services.GetStoreServices;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;

/**
 * Created by Shima on 10/24/2017.
 */

public class StoreActivity extends Activity {

    GetStoreServices storesService;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store);
        recyclerView= (RecyclerView) findViewById(R.id.recyclerView);






        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);


        storesService=new GetStoreServices(this,recyclerView);
        callStoreWebService();

    }
    void callStoreWebService(){
        try {
            storesService.getStores();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }
}
