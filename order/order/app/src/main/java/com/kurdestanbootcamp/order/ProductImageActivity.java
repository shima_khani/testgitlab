package com.kurdestanbootcamp.order;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.kurdestanbootcamp.order.model.Product;
import com.kurdestanbootcamp.order.services.GetProductImageServices;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;

/**
 * Created by Shima on 10/23/2017.
 */


public class ProductImageActivity extends Activity {
    GetProductImageServices productImageServices;
    TextView price, product_name, userId, productid;
    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_image);

        price = (TextView) findViewById(R.id.textView6);
        product_name = (TextView) findViewById(R.id.textView4);
        productid = (TextView) findViewById(R.id.textView7);
        userId = (TextView) findViewById(R.id.textView5);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        Product h = (Product) getIntent().getSerializableExtra("product");
        Log.e("prodctiD", h.getProductid() + "");
        Log.e("product_name", h.getProduct_name() + "");

        product_name.setText(h.getProduct_name()+"");
        price.setText(h.getPrice()+"");
        productid.setText(h.getProductid()+"");
        userId.setText(h.getuserId()+"");
        long productId=h.getProductid();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        productImageServices=new GetProductImageServices(this,recyclerView);
        callImageWebService(productId);

    }
    public void callImageWebService(long productId){

        try {
            productImageServices.getProductImages(productId);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}