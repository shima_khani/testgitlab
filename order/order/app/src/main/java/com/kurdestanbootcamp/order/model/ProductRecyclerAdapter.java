package com.kurdestanbootcamp.order.model;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kurdestanbootcamp.order.ProductActivity;
import com.kurdestanbootcamp.order.R;

import java.util.List;

/**
 * Created by Shima on 10/23/2017.
 */


public class ProductRecyclerAdapter extends RecyclerView.Adapter<ProductViewHolder> {
    private List<Product> productList;
    private ProductActivity mContext;
    int heightSize;
    RecyclerView recyclerView;
    public ProductRecyclerAdapter(ProductActivity context, List<Product> feedItemList,RecyclerView recyclerView) {
        this.productList = feedItemList;
        this.mContext = context;
        this.recyclerView=recyclerView;
        setListener();
        //Log.e   ("const adapter","");
    }

    /*   @Override
       public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
          // View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, null);
           View itemView = LayoutInflater.from(viewGroup.getContext())
                   .inflate(R.layout.item, parent, false);



           CustomViewHolder viewHolder = new CustomViewHolder(view,heightSize);
           Log.e   ("CustomViewHolder adapter","");
           return viewHolder;
       }*/
    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_row, parent, false);


        return new ProductViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final ProductViewHolder viewHolder, final int i) {
        final Product product = productList.get(i);
        viewHolder.price.setText(product.getPrice()+"تومان");
        viewHolder.product_name.setText(product.getProduct_name()+" نام کالا");
        viewHolder.productid.setText(product.getProductid()+"کد کالا");
        viewHolder.product_name.setText(product.getProduct_name());






    }
    public void setListener(){
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(mContext, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int i) {
                        Product h=  productList.get(i);

                        Intent intent=new Intent(mContext, ProductActivity.class)  ;
                        //   intent.putExtra("productId",h.getProductId());
                        intent.putExtra("product",h);
                        mContext.startActivity(intent);

                    }
                })
        );


    }


    @Override
    public int getItemCount() {
        return (null != productList ? productList.size() : 0);

    }


}
