package com.kurdestanbootcamp.order.model;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.Target;
import com.kurdestanbootcamp.order.ProductImageActivity;
import com.kurdestanbootcamp.order.R;
import com.kurdestanbootcamp.order.services.Services;

import java.util.List;

/**
 * Created by Shima on 10/23/2017.
 */

public class ProductImageRecyclerAdapter extends RecyclerView.Adapter<ProductImagesViewHolder> {
    private List<ProductImage> productImageList;
    private ProductImageActivity mContext;
    int heightSize;
    RecyclerView recyclerView;
    public ProductImageRecyclerAdapter(ProductImageActivity context, List<ProductImage> feedItemList) {
        this.productImageList = feedItemList;
        this.mContext = context;


        //Log.e   ("const adapter","");
    }

    /*   @Override
       public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
          // View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, null);
           View itemView = LayoutInflater.from(viewGroup.getContext())
                   .inflate(R.layout.item, parent, false);



           CustomViewHolder viewHolder = new CustomViewHolder(view,heightSize);
           Log.e   ("CustomViewHolder adapter","");
           return viewHolder;
       }*/
    @Override
    public ProductImagesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_image_row, parent, false);


        return new ProductImagesViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final ProductImagesViewHolder viewHolder, final int i) {
        final ProductImage productImage = productImageList.get(i);


        String urlImage= Services.GETPRODUCTIMAGE+productImage.getCounter()+'/'+productImage.getImageId();
        Log.e("img url=",urlImage);

        Glide.with(mContext).load(urlImage)
                .thumbnail(0.5f)
                .crossFade()
                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(viewHolder.imageView);



    }


    @Override
    public int getItemCount() {


        return productImageList.size() ;
    }


}
