package com.kurdestanbootcamp.order;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.kurdestanbootcamp.order.services.GetProductServices;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;

/**
 * Created by Shima on 10/23/2017.
 */


public class ProductActivity extends Activity {

    GetProductServices productsService;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        recyclerView= (RecyclerView) findViewById(R.id.recyclerView);






        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);


        productsService=new GetProductServices(this,recyclerView);
        callProductWebService();

    }
    void callProductWebService(){
        try {
            productsService.getProducts();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }
}
