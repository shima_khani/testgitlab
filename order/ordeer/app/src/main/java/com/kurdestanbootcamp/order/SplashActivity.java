package com.kurdestanbootcamp.order;

/**
 * Created by Shima on 10/16/2017.
 */


import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.kurdestanbootcamp.order.utils.SharedpreferenceHelper;



public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);



        startTimer();
    }

    private void startTimer(){
        new CountDownTimer(4000, 1000) {

            public void onTick(long millisUntilFinished) {

                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                Toast.makeText(SplashActivity.this, "finish", Toast.LENGTH_SHORT).show();
                checkShPrefrenc();
            }

        }.start();
    }
    private void checkShPrefrenc(){
        SharedpreferenceHelper shp=SharedpreferenceHelper.getInstance(this);
        boolean result=shp.alreadySave();
        Log.e("result",result+"");

        if (result){
            startActivity(new Intent(this,MainActivity.class));
        }
        else {

            startActivity(new Intent(this,LoginActivity.class));
        }
        finish();
    }
}

