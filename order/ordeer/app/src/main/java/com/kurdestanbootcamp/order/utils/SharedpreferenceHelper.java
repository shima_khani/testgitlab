package com.kurdestanbootcamp.order.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by tahlilgaran on 10/14/2017.
 */

public class SharedpreferenceHelper {
   static SharedPreferences sharedpreferences;
    public static final String mypreference = "SnaCoursePref";


    private static SharedpreferenceHelper instance;

    private SharedpreferenceHelper(){}

    public static SharedpreferenceHelper getInstance(Context context){
        if(instance == null){
            instance = new SharedpreferenceHelper();

            sharedpreferences = context.getSharedPreferences(mypreference,
                    Context.MODE_PRIVATE);
        }
        return instance;
    }

public boolean alreadySave(){
    if (sharedpreferences.contains("done")) {
     boolean result=   sharedpreferences.getBoolean("done",false);
        return result;
    }
    else {
        return false;
    }
}
public void done(){

    SharedPreferences.Editor editor = sharedpreferences.edit();
    editor.putBoolean("done", true);
   // editor.putString("name", "ali");
   // editor.putLong("id", 1);
    editor.commit();

}


}
