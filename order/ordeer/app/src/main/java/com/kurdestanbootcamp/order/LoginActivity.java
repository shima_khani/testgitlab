package com.kurdestanbootcamp.order;

/**
 * Created by Shima on 10/16/2017.
 */


import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.kurdestanbootcamp.order.services.LoginServices;
import com.kurdestanbootcamp.order.model.User;
import com.kurdestanbootcamp.order.services.LoginServices;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;

public class LoginActivity extends Activity {
    Button btnLogin;
    EditText textName,textPassword,textPhone;

    LoginServices loginServices;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        btnLogin= (Button) findViewById(R.id.btn1);
        textName= (EditText) findViewById(R.id.editText1);
        textPassword= (EditText) findViewById(R.id.editText2);
        textPhone= (EditText) findViewById(R.id.editText3);
        loginServices=new LoginServices(this);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callWebServiceRegister();
            }
        });
    }

    public void callWebServiceRegister(){
        String name=textName.getText().toString();
        String password=textPassword.getText().toString();
        String phone=textPhone.getText().toString();

        User user=new User();
        user.setName(name);
        user.setPassword(password);

        user.setPhone(phone);

        try {
            loginServices.sendToServer(user);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


    }
}
