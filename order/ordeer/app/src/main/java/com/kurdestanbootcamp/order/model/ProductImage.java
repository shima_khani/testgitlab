package com.kurdestanbootcamp.order.model;

/**
 * Created by Shima on 10/23/2017.
 */


public class ProductImage {


    private long imageId;


    private long productId;


    private String path;


    private int counter;

    public long getImageId() {
        return imageId;
    }

    public void setImageId(long imageId) {
        this.imageId = imageId;
    }

    public long getStoreId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }
}
