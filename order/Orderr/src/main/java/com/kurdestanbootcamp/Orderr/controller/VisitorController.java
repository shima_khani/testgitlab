package com.kurdestanbootcamp.Orderr.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.kurdestanbootcamp.Orderr.model.Visitor;
import com.kurdestanbootcamp.Orderr.repository.VisitorDaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class VisitorController {
    @Autowired
    private VisitorDaoService visitorDaoService;
    @RequestMapping(value = "/v1/registervisitor", method = RequestMethod.POST,  consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE},produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public String registerJobSeeker(@RequestBody Map<String, Object> payload) {
        JsonObject jsonObject = new JsonObject();
        try {


            String name = (String) payload.get("name");
            String password = (String) payload.get("password");
            String phone = (String) payload.get("phone");


            System.out.println("1");

            Visitor user = new Visitor();
            user.setVisitorName(name);
            user.setPassword(password);
            user.setPhone(phone);


            Visitor savedUsaer = visitorDaoService.save(user);
            System.out.println("3");
            jsonObject.addProperty("result", true);
            jsonObject.addProperty("id", savedUsaer.getId());
            jsonObject.addProperty("visitorname", savedUsaer.getVisitorName());


            return new Gson().toJson(jsonObject);

        } catch (Exception ex) {

            System.out.println(ex.toString());
            jsonObject.addProperty("reult", false);
            return new Gson().toJson(jsonObject);


        }
    }
}


