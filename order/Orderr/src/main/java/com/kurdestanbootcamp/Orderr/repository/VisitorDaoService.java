package com.kurdestanbootcamp.Orderr.repository;

import com.kurdestanbootcamp.Orderr.model.Visitor;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface VisitorDaoService extends PagingAndSortingRepository<Visitor,Long> {

}
